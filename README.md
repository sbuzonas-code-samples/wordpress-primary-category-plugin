# WordPress Primary Category Plugin

This repository demonstrates a plugin that provides primary category functionality for WordPress. It is a full WordPress installation managed by `docker-compose` including the database data for ease of review.

## Source Code

The code for the plugin is in `public/content/plugins/slb-primary-category`.

## Tooling

The development environment uses some of my own personal tooling to leverage `composer` for dependency management.
