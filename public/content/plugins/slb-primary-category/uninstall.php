<?php
/**
 * Fired with the plugin is uninstalled.
 *
 * @link    https://stevebuzonas.com
 * @since   1.0.0
 * @package Primary_Category
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}
