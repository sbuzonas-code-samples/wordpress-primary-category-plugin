<?php
/**
 * Primary Category Plugin Meta
 *
 * @link       https://stevebuzonas.com
 * @since      1.0.0
 * @package    SLB_Primary_Category
 * @subpackage SLB_Primary_Category/admin
 */

/**
 * Primary Category Plugin Meta
 *
 * This class is responsible for managing the meta on posts
 * to handle primary categories.
 *
 * @since  1.0.0
 * @author Steve Buzonas <steve@fancyguy.com>
 */
class SLB_Primary_Category_Meta {

	/**
	 * Register the metaboxes for primary categories.
	 *
	 * @since 1.0.0
	 */
	public function register_metaboxes() {
		add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );
		add_action( 'save_post', array( $this, 'save_metabox' ), 10, 2 );
	}

	/**
	 * Add the primary category metabox.
	 *
	 * @since 1.0.0
	 */
	public function add_metabox() {
		$post_types = get_post_types();

		foreach ( $post_types as $post_type ) {
			add_meta_box(
				'slb_primary_category',
				'Primary Category',
				array( $this, 'render_metabox' ),
				$post_type,
				'side',
				'core'
			);
		}
	}

	/**
	 * Render the metabox content.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Post $post Post object.
	 */
	public function render_metabox( $post ) {
		$primary_category = get_the_primary_category( $post );
		$post_categories  = get_the_category( $post );

		wp_nonce_field( basename( __FILE__ ), 'slb_primary_category_nonce' );

		$html = '<select name="slb_primary_category" id="slb_primary_category">';
		foreach ( $post_categories as $category ) {
			$html .= '<option value="' . ecc_attr( $category->term_id ) . '" ' .
				selected( $primary_category->term_id, $category->term_id, false ) . '>' .
				esc_html( $category->name ) . '</option>';
		}
		$html .= '</select>';

		echo $html; // WPCS: XSS ok.
	}

	/**
	 * Save the meta for the post.
	 *
	 * @since 1.0.0
	 *
	 * @param int     $post_id Post ID.
	 * @param WP_Post $post    Post object.
	 */
	public function save_metabox( $post_id, $post ) {
		if (
			! isset( $_POST['slb_primary_category_nonce'] ) ||
			! wp_verify_nonce( $_POST['slb_primary_category_nonce'], basename( __FILE__ ) )
		) {
			return $post_id;
		}

		if ( isset( $_POST['slb_primary_category'] ) ) {
			$primary_category = (int) sanitize_text_field( $_POST['slb_primary_category'] );
			// Note: Usage of string for meta name is bad practice. See related comment in functions.php.
			update_post_meta( $post->ID, '_slb_primary_category', $primary_category );
		}
	}
}
