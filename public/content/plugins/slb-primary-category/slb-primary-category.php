<?php
/**
 * Primary Category
 *
 * This is the main entrypoint for the plugin.
 *
 * @link        https://stevebuzonas.com
 * @since       1.0.0
 * @package     SLB_Primary_Category
 *
 * @wordpress-plugin
 * Plugin Name: Primary Category
 * Plugin URI:  https://gitlab.com/sbuzonas/10up-challenge
 * Description: Adds a theme feature for primary categories.
 * Version:     1.0.0
 * Author:      Steve Buzonas
 * Author URI:  https://stevebuzonas.com
 * Text Domain: slb-primary-category
 * Domain Path: /languages
 */

if ( ! defined( 'WPINC' ) ) {
	exit;
}

define( 'SLB_PRIMARY_CATEGORY_VERSION', '1.0.0' );

require plugin_dir_path( __FILE__ ) . 'includes/class-slb-primary-category-initializer.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-slb-primary-category-plugin.php
 */
function slb_primary_category_activate() {
	SLB_Primary_Category_Initializer::activate();
}
register_activation_hook( __FILE__, 'slb_primary_category_activate' );

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-slb-primary-category-plugin.php
 */
function slb_primary_category_deactivate() {
	SLB_Primary_Category_Initializer::deactivate();
}
register_deactivation_hook( __FILE__, 'slb_primary_category_deactivate' );

/**
 * The code that runs during plugin initialization.
 * This action is documented in includes/class-slb-primary-category-plugin.php
 */
function slb_primary_category_initialize() {
	$plugin = SLB_Primary_Category_Initializer::get_instance();
	$plugin->initialize();
}
slb_primary_category_initialize();

require plugin_dir_path( __FILE__ ) . 'includes/functions.php';
