<?php
/**
 * Primary Category Plugin Initializer
 *
 * @link       https://stevebuzonas.com
 * @since      1.0.0
 * @package    SLB_Primary_Category
 * @subpackage SLB_Primary_Category/includes
 */

/**
 * Primary Category Plugin Initializer
 *
 * This class is responsible for managing the lifecycle events
 * of the plugin. It should include all of the WordPress hooks
 * for a simple way of identifying plugin entrypoints.
 *
 * @since  1.0.0
 * @author Steve Buzonas <steve@fancyguy.com>
 */
class SLB_Primary_Category_Initializer {

	/**
	 * Singleton instance
	 *
	 * @since 1.0.0
	 * @var   SLB_Primary_Category_Initializer
	 */
	private static $instance;

	/**
	 * Entrypoint for plugin activation.
	 *
	 * @since 1.0.0
	 */
	public static function activate() {
	}

	/**
	 * Entrypoint for plugin deactivation.
	 *
	 * @since 1.0.0
	 */
	public static function deactivate() {
	}

	/**
	 * Singleton.
	 *
	 * @since 1.0.0
	 * @return SLB_Primary_Category_Initializer
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * Constructor.
	 *
	 * Private to force singleton pattern.
	 *
	 * @since 1.0.0
	 */
	private function __construct() {
		$this->plugin_path = plugin_dir_path( dirname( __FILE__ ) );
		$this->plugin_url  = plugin_dir_url( dirname( __FILE__ ) );
		$this->version     = SLB_PRIMARY_CATEGORY_VERSION;

		$this->load_dependencies();
	}

	/**
	 * Entrypoint for plugin initialization.
	 *
	 * Plugin is still loading at this time.
	 * Scope of execution should be limited to
	 * registration of hooks and filters.
	 *
	 * @since 1.0.0
	 */
	public function initialize() {
		if ( is_admin() ) {
			$this->register_admin_hooks();
		} else {
			$this->register_public_hooks();
		}
		$this->register_shared_hooks();
	}

	/**
	 * Loads the dependencies required for the current request state.
	 *
	 * @since 1.0.0
	 */
	protected function load_dependencies() {
		$this->load_shared_dependencies();
		if ( is_admin() ) {
			$this->load_admin_dependencies();
		} else {
			$this->load_public_dependencies();
		}
	}

	/**
	 * Loads the dependencies required for the admin side.
	 *
	 * @since 1.0.0
	 */
	protected function load_admin_dependencies() {
		$this->load_plugin_file( 'admin/class-slb-primary-category-meta.php' );
	}

	/**
	 * Loads the dependencies required for the public side.
	 *
	 * @since 1.0.0
	 */
	protected function load_public_dependencies() {
	}

	/**
	 * Loads the dependencies shared between admin and public.
	 *
	 * @since 1.0.0
	 */
	protected function load_shared_dependencies() {
	}

	/**
	 * Load a file from relative to the plugin root.
	 *
	 * @since 1.0.0
	 *
	 * @param string $path The path to the file to be loaded. Relative from the plugin root.
	 */
	final protected function load_plugin_file( $path ) {
		require $this->plugin_path . $path;
	}

	/**
	 * Register hooks for the admin side.
	 *
	 * @since 1.0.0
	 */
	protected function register_admin_hooks() {
		$admin_meta = new SLB_Primary_Category_Meta();

		add_action( 'load-post.php', array( $admin_meta, 'register_metaboxes' ) );
		add_action( 'load-post-new.php', array( $admin_meta, 'register_metaboxes' ) );
	}

	/**
	 * Register hooks for the public side.
	 *
	 * @since 1.0.0
	 */
	protected function register_public_hooks() {
	}

	/**
	 * Register hooks shared between admin and public.
	 *
	 * @since 1.0.0
	 */
	protected function register_shared_hooks() {
	}
}
