<?php
/**
 * Primary Category Functions
 *
 * All global functions should be defined in this file.
 *
 * @link       https://stevebuzonas.com
 * @since      1.0.0
 * @package    Primary_Category
 * @subpackage Primary_Category/includes
 */

// phpcs:disable WordPress.NamingConventions.PrefixAllGlobals

// Ideally these template tags would be encapsulated within a term class
// and just wrap calls to the class procedurally for ease of use for
// template creators.
/**
 * Retrieve post primary category.
 *
 * This tag may be used outside The Loop by passing a post id as the parameter.
 *
 * Note: This would be much cleaner to be done at the term level, but for the
 * sake of brevity we only handle category as per requirements.
 *
 * @since 1.0.0
 *
 * @param int $id Optional, default to current post ID. The post ID.
 * @return WP_Term The primary category assigned to the post.
 */
function get_the_primary_category( $id ) {
	$post = get_post( $id );

	if ( ! $post ) {
		return false;
	}

	// Note: The use of a string for the meta field in this direct implementation
	// should be avoided and delegated to a class responsible for post meta.
	$cat_id   = (int) get_post_meta( $post->ID, '_slb_primary_category', true );
	$category = get_term( $cat_id );

	// Note: This behavior will fallback to the first category, this may not be
	// the desired behavior. Should implement a better heuristic.
	if ( is_wp_error( $category ) ) {
		$categories = get_the_category( $post->ID );
		$category   = array_shift( $categories );
	}

	/**
	 * Filters the primary category to return for a post.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Term $category The primary category to return for the post.
	 * @param int     $id       ID of the post.
	 */
	return apply_filters( 'get_the_primary_category', $category, $id );
}

/**
 * Display primary category for the post.
 *
 * @since 1.0.0
 *
 * @param int $post_id Optional. Post ID to retrieve primary category.
 */
function the_primary_category( $post_id = false ) {
	global $wp_rewrite;

	/**
	 * Filters the primary category before building the category output.
	 *
	 * @since 1.0.0
	 *
	 * @param WP_Term  $category The post's primary category.
	 * @param int|bool $post_id  ID of the post we're retrieving the primary category for. When `false`, we assume the
	 *                           current post in the loop.
	 */
	$category = apply_filters( 'the_primary_category', get_the_primary_category( $post_id ), $post_id );

	$rel = ( is_object( $wp_rewrite ) && $wp_rewrite->using_permalinks() ) ? 'rel="category tag"' : 'rel="category"';

	$output = '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>' . $category->name . '</a>';

	echo $output; // WPCS: XSS OK.
}

// phpcs:enable WordPress.NamingConventions.PrefixAllGlobals
